#! /bin/sh

n=0
t=0
c=0

setN () {
	echo $1
	n=$1
	echo $n
	}

shoot () {
	echo "$n photos"
	echo "$t exposure"

##echo $var


#for ((c=0; c<n; c++)); do
for i in $(seq 0 $n); do

#var="gphoto2 --force-overwrite --filename \"Canon_$c.jpg\" --set-config eosremoterelease=Immediate --wait-event=${t}s --set-config eosremoterelease=Off --capture-tethered"
#eval $var
##./usbReset.sh

./usbReset.sh

done

#./usbReset.sh

}

showopts () {
  while getopts ":p:q:n:c:t:" optname
    do
      case "$optname" in
        "p")
          echo "Option $optname is specified"
          ;;
        "q")
          echo "Option $optname has value $OPTARG"
          ;;
        "?")
          echo "Unknown option $OPTARG"
		  echo "-t 	Exposure in seconds value between pictures."
		  echo "-n 	Number of Images to take."
		  echo "-c 	Staring Number for image names."
		  #echo "No argument value for option $OPTARG"
          ;;
        ":")
          echo "No argument value for option $OPTARG"
          ;;
		 "n")
         echo "$OPTARG"
		 if [ "$OPTARG" -eq "$OPTARG" ] 2>/dev/null; then
			#echo number
			n="$OPTARG"
			#setN $OPTARG
			
		 else
			echo not a number
		 fi
          ;;
		 "t")
         echo "$OPTARG"
		 if [ "$OPTARG" -eq "$OPTARG" ] 2>/dev/null; then
			#echo number
			t="$OPTARG"
		 else
			echo not a number
		 fi
          ;;
		 "c")
         echo "No argument value for option $OPTARG"
		 if [ "$OPTARG" -eq "$OPTARG" ] 2>/dev/null; then
			#echo number
			c=$OPTARG
		 else
			echo not a number
		 fi
          ;;
        *)
        # Should not occur
          echo "Unknown error while processing options"
          ;;
      esac
    done
	shoot
  return $OPTIND
}


optinfo=$(showopts "$@")
#argstart=$?
#arginfo=$(showargs "${@:$argstart}")
#echo "Arguments are:"
#echo "$arginfo"
echo "Options are:"
echo "$optinfo"
